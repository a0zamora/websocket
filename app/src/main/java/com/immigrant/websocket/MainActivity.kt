package com.immigrant.websocket

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.immigrant.websocket.interactor.MainInteractor
import com.immigrant.websocket.provider.WebServicesProvider
import com.immigrant.websocket.repository.MainRepository
import com.immigrant.websocket.viewmodel.MainViewModel

class MainActivity : AppCompatActivity() {
    private val viewModel= MainViewModel(MainInteractor(MainRepository(WebServicesProvider())))
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<TextView>(R.id.textView).setOnClickListener {
             viewModel.subscribeToSocketEvents()
        }

        findViewById<Button>(R.id.button).setOnClickListener {
            viewModel.sendMessage("{\"type\": \"say\", \"data\": \"Hi to all\"}")
        }
    }
}
package com.immigrant.websocket.provider.dto

class SocketAbortedException : Exception()
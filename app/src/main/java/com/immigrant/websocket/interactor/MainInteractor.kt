package com.immigrant.websocket.interactor

import com.immigrant.websocket.provider.SocketUpdate
import com.immigrant.websocket.repository.MainRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel

class MainInteractor constructor(private val repository: MainRepository) {

    @ExperimentalCoroutinesApi
    fun stopSocket() {
        repository.closeSocket()
    }

    @ExperimentalCoroutinesApi
    fun startSocket(): Channel<SocketUpdate> = repository.startSocket()

    @ExperimentalCoroutinesApi
    fun sendMessage(text: String) = repository.sendMessage(text)

}
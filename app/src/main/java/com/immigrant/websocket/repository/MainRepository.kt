package com.immigrant.websocket.repository

import com.immigrant.websocket.provider.SocketUpdate
import com.immigrant.websocket.provider.WebServicesProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel

class MainRepository constructor(private val webServicesProvider: WebServicesProvider) {

    @ExperimentalCoroutinesApi
    fun startSocket(): Channel<SocketUpdate> =
        webServicesProvider.startSocket()

    @ExperimentalCoroutinesApi
    fun closeSocket() {
        webServicesProvider.stopSocket()
    }

    @ExperimentalCoroutinesApi
    fun sendMessage(text: String) {
        webServicesProvider.sendMessage(text)
    }
}